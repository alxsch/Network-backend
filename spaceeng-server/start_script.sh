#!/bin/bash

useradd -m steam
cd /home/steam
add-apt-repository multiverse
dpkg --add-architecture i386
apt update
apt install lib32gcc1 steamcmd
mkdir /user/games/steamcmd && ln -s /usr/games/steamcmd steamcmd
